﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Greentax.Controllers;
using System.Net;
using Newtonsoft.Json;
using Greentax.Models;

namespace Greentax.Tests.Controllers
{
    [TestClass]
    public class GreentaxControllerTest
    {
        private GreentaxController controller = SetupController();

        [TestMethod]
        public void Get()
        {
            // Act
            var result = controller.Get();

            // Assert
            Assert.IsNotNull(result);

            List<GreenTaxModel> lstGreenTaxModel = (List<GreenTaxModel>)JsonConvert.DeserializeObject(result.Content.ReadAsStringAsync().Result, typeof(List<GreenTaxModel>));

            Assert.AreEqual(2, lstGreenTaxModel.Count);
        }

        [TestMethod]
        public void GetByMileageVehicleTypeFuelTypeAndRegistrationYear()
        {
            //Act
            var result = controller.Get(19, "personbil", "benzin", 2018);

            GreenTaxModel greenTaxModel = (GreenTaxModel)JsonConvert.DeserializeObject(result.Content.ReadAsStringAsync().Result, typeof(GreenTaxModel));

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.StatusCode, HttpStatusCode.OK);
            Assert.AreEqual(greenTaxModel.greenTax, 600);
        }

        [TestMethod]
        public void TestInvalidVehicleType()
        {
            //Act
            var result = controller.Get(19, "lastbil", "benzin", 2018);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(result.StatusCode, HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public void TestInvalidFuelType()
        {
            //Act
            var result = controller.Get(15, "personbil", "diesel", 2018);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(result.StatusCode, HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public void TestInvalidRegistrationYear()
        {
            //Act
            var result = controller.Get(15, "personbil", "benzin", 2028);

            //Assert
            Assert.IsNotNull(result);
            // "Invalid" registration year means no result
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(result.StatusCode, HttpStatusCode.NotFound);
        }

        private static GreentaxController SetupController()
        {
            //Arrange
            var context = new TestGreentaxDatabaseContext();

            foreach (var greentax in GetTestGreentaxes())
            {
                context.GreenTax.Add(greentax);
            }

            var controller = new GreentaxController(context);

            var request = new HttpRequestMessage();
            controller.Request = request;
            var configuration = new HttpConfiguration();
            controller.Configuration = configuration;
            request.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = configuration;

            return controller;
        }

        private static List<GreenTax> GetTestGreentaxes()
        {
            var lstGreenTax = new List<GreenTax>();

            lstGreenTax.Add(new GreenTax() { FirstRegistration = 2018, Fuel = "benzin", Id = 0, IntervalMax = 19.9m, IntervalMin = 18.2m, Rate = 600, VehicleType = "personbil" });
            lstGreenTax.Add(new GreenTax() { FirstRegistration = 2017, Fuel = "benzin", Id = 1, IntervalMax = 18.1m, IntervalMin = 17.9m, Rate = 600, VehicleType = "personbil" });

            return lstGreenTax;
        }
    }
}
