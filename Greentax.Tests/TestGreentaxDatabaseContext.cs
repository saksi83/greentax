﻿using Greentax.Models;
using System.Data.Entity;

namespace Greentax.Tests
{
    public class TestGreentaxDatabaseContext : IGreentaxDatabaseContext
    {
        public DbSet<GreenTax> GreenTax { get; set; }

        public TestGreentaxDatabaseContext()
        {
            this.GreenTax = new TestGreentaxDbSet();
        }

        public void Dispose()
        {
        }

        public void MarkAsModified(GreenTax item)
        {
        }

        public int SaveChanges()
        {
            return 0;
        }
    }
}
