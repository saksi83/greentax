﻿using Newtonsoft.Json;

namespace Greentax.Models
{
    public class GreenTaxModel
    {
        public GreenTaxModel()
        {

        }

        public GreenTaxModel(int rate)
        {
            greenTax = rate;
        }

        public int greenTax;
        [JsonIgnore]
        public decimal IntervalMin;
        [JsonIgnore]
        public decimal IntervalMax;

        public string Interval
        {
            get
            {
                 return string.Format("{0} - {1} km/l", IntervalMin, IntervalMax); ;
            }
        }
    }
}