﻿using System;
using System.Data.Entity;

namespace Greentax.Models
{
        public interface IGreentaxDatabaseContext : IDisposable
        {
            DbSet<GreenTax> GreenTax { get; }
            int SaveChanges();
            void MarkAsModified(GreenTax item);
    }
}