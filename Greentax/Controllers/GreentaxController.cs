﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Greentax.Models;

namespace Greentax.Controllers
{
    public class GreentaxController : ApiController
    {
        #region Constructor

        private IGreentaxDatabaseContext db = new GreenTaxDatabaseEntities();

        public GreentaxController()
        {

        }

        public GreentaxController(IGreentaxDatabaseContext context)
        {
            db = context;
        }

        #endregion

        public HttpResponseMessage Get()
        {
            var greenTaxes = new List<GreenTaxModel>();

                greenTaxes = (from greenTax in db.GreenTax
                              select new GreenTaxModel
                              {
                                  IntervalMax = greenTax.IntervalMax,
                                  IntervalMin = greenTax.IntervalMin
                              }).ToList();

            var response = Request.CreateResponse(HttpStatusCode.OK, greenTaxes);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            return response;
        }

        //   public async Task<IHttpActionResult> Register(RegisterBindingModel model) 

        // GET 
        public HttpResponseMessage Get(int kmprliter, string type, string fuel, int registrationYear)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);

            //Check of vehicle type
            if (!type.ToLower().Equals("personbil"))
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError("Only type=personbil is currenctly supported"));
            }

            //Check of fuel type
            if (!fuel.ToLower().Equals("benzin"))
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError("Only fuel=benzin is currenctly supported"));
            }

            if(response.StatusCode != HttpStatusCode.BadRequest)
            {
                var greentax = db.GreenTax.FirstOrDefault(x => x.VehicleType == type && kmprliter < x.IntervalMax && kmprliter > x.IntervalMin && x.Fuel == fuel);

                if (greentax != null)
                {
                    var greenTaxModel = new GreenTaxModel { greenTax = greentax.Rate, IntervalMin = greentax.IntervalMin, IntervalMax = greentax.IntervalMax };

                    response = Request.CreateResponse(HttpStatusCode.OK, greenTaxModel);
                }
                else
                {
                    response = Request.CreateErrorResponse(HttpStatusCode.NotFound, new HttpError("No matching rate found"));
                }
            }

            return response;
        }
    }
}